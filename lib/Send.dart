import 'package:flutter/material.dart';

class Send extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new Container(
      child: new Center(
        child: new Icon(Icons.send, size: 150.0, color: Colors.black)
      )
    );
  }
}